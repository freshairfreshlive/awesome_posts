## Day 2 -搭建WPF RSS Reader雏形

有了Day 1的知识铺垫以后，今天我们将编写WPF RSS Reader软件的雏形，在开始之前，我们需要了解以下几个工具:

* vs2013：这个工具经常用于编写C#源码，设计界面(也就是编写.Xaml文件)，编译源码，调试源码，是一个提供给开发者的集成环境
* git：这个是分布式版本库，主要用来记录我们编写源码的版本，以及我们能够回退到某一个版本
* Bitbucket：这是一个源码托管服务，我们一般会用git来将修改的源码上传到Bitbucket所提供的服务器上，也可以从服务器上获取源码，通过这种托管源代码的机制，我们就可以在不同的地方，不同人，不同时间，编写和分享源码，
* MVVMLight：MVVM是一个设计模式，为了实现MVVM模式，我们必须借助一些现有的框架来支持我们的MVVM模式，而MVVMLight就是这种框架，借助这个框架，我们可以很方便的利用WPF的Binding以及MVVMLight提供的模块来轻松的实现MVVM模式

**注意：确保以上概念清楚，然后把工具安装好再继续学习**

##一步一步来实现

* 启动vs2013，按照下图新建一个solution和projects，务必和下图一样

![](http://7xi3xu.com1.z0.glb.clouddn.com/Day2_new_solution.png)

* 通过Manager NuGet Packages为RSSFeedReader，RSSFeedReader.Dialogs，RSSFeedReader.ViewModels添加MVVM Light引用，为RSSFeedReader.Data添加Microsoft.CompilerServices.AsyncTargetingPack引用
  * RSSFeedReader中的View就代表MVVM中的View
  * RSSFeedReader.Data中的Models就代表了MVVM中的Model
  * RSSFeedReader.ViewModels代表了MVVM中的ViewModel
  * 其它RSSFeedReader模块提供了通用功能，我们先不要深究

* 以上2步完成以后再进入到solution文件所在的目录，可以看到该目录下将包含红色框中的内容

![](http://7xi3xu.com1.z0.glb.clouddn.com/Day2_solution_dir.png)

* 回到vs2013，根据下面每个模块的依赖关系图，为新建的solution中的projects建立依赖关系，有箭头一端的是被依赖的，比如RSSFeedReader依赖于RSSFeedReader.ViewModels。

![](http://7xi3xu.com1.z0.glb.clouddn.com/Day2_project_relationship.png)

* 到[这里](https://bitbucket.org/freshairfreshlive/wpf_rss_reader_tutorial/src/d40fd5344f0154e952050d323c4ff61a114ca796/RSSFeedReader/Dependencies/?at=Day2)去下载**Interop.SHDocVw.dll**，**SingleInstanceApplication.dll**文件，在solution所在的目录创建一个Dependencies目录，并将这些文件拷贝到目录Dependencies里，然后通过vs2013根据以下依赖关系图为solution中的projects建立依赖关系

![](http://7xi3xu.com1.z0.glb.clouddn.com/Day2_3rd_ref_dll.png)

* 到Bitbucket中注册一个账号，新建一个repository名字叫WPFRSSReader
* 打开git bash，按照以下几步完成本地与服务端(前面一步新建的repository名字叫WPFRSSReader)
  * 使用```git clone```命令将Bitbucket中WPFRSSReader获取到本地
  * 将创建好的solution拷贝到本地WPFRSSReader目录下面
  * 使用```git add .```来把当前目录下的所有文件（包括子目录）添加到git的stage area等待下一步提交
  * ```git commit -m 'add WPF RSS Feed solution' ```提交stage area文件到本地的git版本控制系统，提交信息为add WPF RSS Feed solution
  * ```git push origin master```我们还需把之前提交到本地的修改，推送到申请的Bitbucket repository，经过这一步我们就可以将我们的代码托管到了Bitbucket服务了，同时我们也可以在其他地方访问我们修改过后的代码了

通过以上步骤，我们最终要达到的目的有：

* 1.将新建的WPFRSSReader上传到Bitbucket服务器
* 2.创建了一个WPFRSSReader solution，并且可以编译生成可执行性文件，运行这个可执行性文件将得到下图红框中的应用窗口Window1

![](http://7xi3xu.com1.z0.glb.clouddn.com/Day2_final_result.png)

如果你无法得到上图的执行结果，那么你可以点击[这里](https://bitbucket.org/freshairfreshlive/wpf_rss_reader_tutorial/src/5e81c651c99915b429fff4b57bc56fc4366df5df/?at=Day2)或者通过以下git命令方式来获取这节课的代码

```git clone https://freshairfreshlive@bitbucket.org/freshairfreshlive/wpf_rss_reader_tutorial.git -b Day2```

**注意：请先理解以上概念之后再到下一篇文章学习**

##关于WPF相关的学习资料

我们已经将WPF学习的资料放到DropBox里了，你可以点击[这里](https://www.dropbox.com/s/elasm0oag31flfs/wpf_books.7z?dl=0)去下载。另外学习技术知识最好是读英文，多看国外的文章，为此你要准备时时使用google搜索，google搜索不仅可以帮我们查找到想要的文章，最重要的是它会很准。你也可以使用[这些](https://en.wikipedia.org/wiki/List_of_search_engines)搜索引擎。如果你们是google的追求者那么我们可以考虑[www.nohabitat.com](www.nohabitat.com)。

##成立学习小组

如果你觉得我们团队分享的知识能够让你成长，进而想了解到更多的技术知识或者企业开发流水线，我们会考虑设立**付费项目**，一共收纳3个学生，提供1个导师，组成一个4人团队，最终选择一个热门项目实战，让你能够在较短的时间内，掌握尽可能多的知识，同时培养团队合作意识，进而提高你进入软件行业或者跳槽到软件行业工作的机会

##关于作者

```javaScript
  var air = {
    nickName : "air",
    location : "浦东,上海",
    we-chat : "18717942545",
    phone : "18717942545",
    email : "freshairfreshlive@gmail.com"
  }
```
[回到学习路线图](https://zhuanlan.zhihu.com/p/21794790)

[跳到Day 2](https://zhuanlan.zhihu.com/p/24020180) ===================[跳到Day 3](https://zhuanlan.zhihu.com/p/21794790)