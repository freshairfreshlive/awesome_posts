﻿
##WPF RSS Reader 是什么?

WPF RSS Reader是一个RSS订阅软件，该软件依赖.NET，由C#编写而成，是一个快速学习软件研发的轻量级项目。

##这个项目的目的

我们准备通过设立这个轻量级的项目让你们了解到研发一个企业软件需要用到的工具，技术，以及软件设计的思想。其中`思想最重要`，其次是技术，最后是工具。
通过这个轻量级项目的学习，你们将学会以下几点

* 工具：vs2013，git
* 技术：WPF,C#,MVVM
* `软件设计思想：划分模块`

**注意：请先把vs2013，git安装好**

##关于WPF相关的学习资料

我们已经将WPF学习的资料放到DropBox里了，你可以点击[这里](https://www.dropbox.com/s/elasm0oag31flfs/wpf_books.7z?dl=0)去下载。另外学习技术知识最好是读英文，多看国外的文章，为此你要准备时时使用google搜索，google搜索不仅可以帮我们查找到想要的文章，最重要的是它会很准。你也可以使用[这些](https://en.wikipedia.org/wiki/List_of_search_engines)搜索引擎。如果你们是google的追求者那么我们可以考虑[www.nohabitat.com](www.nohabitat.com)。

##成立学习小组

如果你觉得我们团队分享的知识能够让你成长，进而想了解到更多的技术知识或者企业开发流水线，我们会考虑设立**付费项目**，一共收纳3个学生，提供1个导师，组成一个4人团队，最终选择一个热门项目实战，让你能够在较短的时间内，掌握尽可能多的知识，同时培养团队合作意识，进而提高你进入软件行业或者跳槽到软件行业工作的机会

##关于作者

```javaScript
  var air = {
    nickName : "air",
    location : "浦东,上海",
    we-chat : "18717942545",
    phone : "18717942545",
    email : "freshairfreshlive@gmail.com"
  }
```
[回到学习路线图](https://zhuanlan.zhihu.com/p/21794790)
[Day 1](https://zhuanlan.zhihu.com/p/21794790)