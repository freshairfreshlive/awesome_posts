﻿## Day 1 -预备知识了解

编写WPF RSS Reader软件之前，需要理解以下几个技术知识点:

* .Net
* WPF
* MVVM
* C#
* Xaml文件

##如何把以上技术点整合

C#是一门面向对象的编程语言，一般需要通过vs2013 IDE编写C#代码，然后编译出可执行的桌面应用。

.NET是微软提供给开发者的框架，这个框架提供了各种子框架，比如WPF，WCF。使用框架的目的是简化加速开发。

WPF是依赖于.NET上的子框架，我们通过WPF可以快速开发出非常酷炫的桌面应用。

MVVM是一种开发应用的思维模式，全称为**M**odel-**V**iew-**V**iewModel，该模式不仅可以应用到桌面应用的开发，同时也可以应用到Web应用的开发。

Xaml文件是一种WPF框架中用来定义界面的文件，一般我们会将我们想显示的界面元素定义在这个文件里，界面元素我们一般称为控件，比如Menu控件。

有了以上简要的说明，我们将以上技术点一一对应到我们要做的WPF RSS Reader这个应用当中。我们要做的WPF RSS Reader主界面如下图所示:

![](http://7xi3xu.com1.z0.glb.clouddn.com/MainView.png)

分为4部分，它们分别是:

* 1.WPF提供的菜单控件
* 2.ChannelView：自定义的控件，用于显示有订阅了几个RSS 频道
* PostView：自定义的控件，用于显示选中频道包含了几篇文章
* WebBrowser：WPF提供的浏览器显示页面控件

有了以上设计之后，我们将准备实现它们。

* 1.选择所依赖的技术：我们需要在vs2013里创建一个solution,在这个solution里添加多个Project，与界面相关的Project创建的时候应该选择WPF类型。
* 2.应用MVVM模式分层：一共分层三层，它们分别为
  * View：专门实现界面的一层。比如上图的2.ChannelView就是其中一个View
  * Model：现实世界的抽象。比如WPF RSS Reader需要显示RSS频道，那么这个RSS频道就会通过C#来抽象定义出一个Channel，用这个Channel来表示现实世界中的RSS频道
  * ViewModel：这一层是连接View与Model的桥梁，许多逻辑操作都将放到这一层完成，比如，添加一个RSS Channel，这个添加操作需要放到这一层来实现
* 3.根据第2步的结果，用Xaml文件来实现View

现在我们回过头来想想以上3步如何使用到了本文提到的5项技术。

* 1.选择所依赖的技术这步就决定了你使用了.NET,WPF,C#技术
* 2.应用MVVM模式分层这步就决定了你使用了MVVM设计模式
* 3.Xaml文件是实现WPF应用界面的技术之一

因此通过以上3步之后你将会明白，WPF RSS Reader将会有很多个View，很多个Model，很多个ViewModel，我们用View代表所有的View，用Model代表所有的Model，用ViewModel代表所有的ViewModel，因此就有了**MVVM**模式。
下图包含了WPF RSS Reader大部分的View，Model，ViewModel，以及它们之间的关系。

![](http://7xi3xu.com1.z0.glb.clouddn.com/class_diagram.png)

**注意：请先理解以上概念之后再到下一篇文章学习**

##关于WPF相关的学习资料

我们已经将WPF学习的资料放到DropBox里了，你可以点击[这里](https://www.dropbox.com/s/elasm0oag31flfs/wpf_books.7z?dl=0)去下载。另外学习技术知识最好是读英文，多看国外的文章，为此你要准备时时使用google搜索，google搜索不仅可以帮我们查找到想要的文章，最重要的是它会很准。你也可以使用[这些](https://en.wikipedia.org/wiki/List_of_search_engines)搜索引擎。如果你们是google的追求者那么我们可以考虑[www.nohabitat.com](www.nohabitat.com)。

##成立学习小组

如果你觉得我们团队分享的知识能够让你成长，进而想了解到更多的技术知识或者企业开发流水线，我们会考虑设立**付费项目**，一共收纳3个学生，提供1个导师，组成一个4人团队，最终选择一个热门项目实战，让你能够在较短的时间内，掌握尽可能多的知识，同时培养团队合作意识，进而提高你进入软件行业或者跳槽到软件行业工作的机会




##关于作者

```javaScript
  var air = {
    nickName : "air",
    location : "浦东,上海",
    we-chat : "18717942545",
    phone : "18717942545",
    email : "freshairfreshlive@gmail.com"
  }
```
[回到学习路线图](https://zhuanlan.zhihu.com/p/21794790)
[回到开始<](https://zhuanlan.zhihu.com/p/24020180)
[>跳到Day 2](https://zhuanlan.zhihu.com/p/21794790)