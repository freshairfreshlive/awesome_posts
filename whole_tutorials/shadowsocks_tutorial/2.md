# Chapter 3. shadowsocks是什么

这一章中我们主要分以下几点来讲解：

1. shadowsocks能为我们做什么
2. shadowsocks由哪些子系统组成
3. 总结

shadowsocks的中文翻译是影梭，它是一款基于socks5协议，并对传输的数据进行加密／解密的网络应用。在之后的文章，我将介绍Python版本的shadowsocks。分析shadowsocks的源码之前，我们应知道shadowsocks能够帮助我们访问google等之类的网站；为了使用shadowsocks服务，我们需要安装shaodowsocks客户端（Window，Mac，IOS，Android），接着应该拥有一个服务器ip，端口号，密码，用于连接shadowsocks服务器，这个shadowsocks服务器可以到[这里](http://www.nohabitat.com)购买,也可以自己搭建；相比VPN，shadowsocks的信息传输没有明显的特征，这使得它很容易躲过防火墙的围堵。

## 3.1 shadowsocks能为我们做什么
***

随着学校，公司，研究机构，政府等局域网纷纷加入互联网这个大家庭，互联网包含的信息越来越丰富，这使得个人很容易从互联网获取信息。以前我们只要将个人的PC用一根网线与互联网连接，我们就可以轻易浏览某些公司的网站。但是现在我们的互联网局部受到限制，就导致了即使个人PC通过一根网线连接互联网，以前可以浏览的信息，现在不行了。究其原因，其实是数据传输过程中遇到了干扰啊，为了不让数据传输受到干扰，我们很有技巧的将shadowsocks放置在数据传输的中间，使得传输恢复了正常，以前能访问的网站，现在也可以了。

### 搜索

每天我们总会面对诸多互联网信息，当信息呈现出爆炸式增长时，如何获取对我们有利的信息就显得十分重要。因此我们要借助搜索，比如国外的google以及国内的baidu。而shadowsocks就是一个这样一个服务，能够帮助我们访问google搜索。

### Youtobe

互联网不仅提供了文字图片信息，还提供了娱乐信息，比如说我们常见的娱乐节目，而这些娱乐信息又必须依托于网络视频的平台，比如Youtobe。而shadowsocks就是这样一个服务，能够帮助我们快速的访问youtobe视频娱乐平台。

### 各大社交

互联网的属性之一就是连接，而人在网络上的社交就是在这一连接的基础上，添加了个人的感情因素形成的。社交作为人基本属性之一，每个人每天都会面对。因此我们应该充分利用社交平台，比如facebook之类的平台。而shadowsocks就是这样一个服务，能够帮助我们在合适的时机玩转社交。

### 其它

此外我们还有大量的需求，比如说做外贸，看漫画，玩游戏，做设计，应用学术等均会需要shadowsocks的支持。

## 3.2 shadowsocks由哪些子系统组成
***

shadowsocks是一个拥有2部分的系统，如下图所示

![](day2_ss_subsystems.png)

我们把sslocal看成客户端，ssserver看做服务端。一般shadowsocks服务提供商只会提供ssserver，这部分不需要用户了解。而用户主要使用的是sslocal，在日常使用中，用户一般会在本机上安装具有图形界面的sslocal，因为大部分的用户不习惯在命令行中启动sslocal。

## 3.3 总结
***
通过学习本章内容，读者应该要知道以下几点

* shadowsocks有很多版本的实现，以后分析我将分析Python版的源码
* shadowsocks能够提高我们日常生活的效率，比如搜索信息，随时满足我们的社交需求等
* shadowsocks分为sslocal和ssserver两部分，作为用户我们应该只关注sslocal的安装和使用；而作为工程师，我们必须同时关注sslocal和ssserver部分。