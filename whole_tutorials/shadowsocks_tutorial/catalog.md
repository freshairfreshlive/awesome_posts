## 目录

* 前言-如何快速理解我在这个系列里的思想
* day 0 python可以用来做什么
* day 1 基础知识准备
* day 2 shadowsocks是什么
* day 3 了解sock5协议
* day 4 解开shadowsocks神秘的面纱-窥视架构图
* day 5 local和server
* day 6 shell-配置文件解析模块
* day 7 daemon-守护者模块
* day 8 encrypt-加密/解密模块
* day 9 lru_cache-LRU缓存模块
* day 10 manager-多用户管理模块
* day 11 evenloop-异步io单线程处理模块
* day 12 asyncdns-异步dns请求模块
* day 13 tcprelay和TCPRelayHandler-tcp连接转发模块
* day 14 udprelay-udp连接转发模块
* day 15 回顾