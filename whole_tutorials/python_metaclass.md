# Python metaclass usage
我们知道Python是脚本语言，所以Python拥有一些特性是静态语言没有的。今天我们将花一点时间来了解Python语言中的某个特性：**__metaclass__**。

这个特性的作用是让我们能够在运行时定义一个class。
我们要用这个特性来编写我们的ORM模块！
所以要看懂这些代码，首先需要了解如何使用Python的metaclass。

#Python datetime.tzinfo 使用
tzinfo只能继承，不能实例化。继承之后需要的类需要实例化传入给datetime或date类型使用。同时需要了解UTC+(hhmm),UTC-(hhmm)区别