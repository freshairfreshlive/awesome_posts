# c++面试前总结

C++这门语言很任性啊，不仅像C语言一样需要从头搭建自己的功能包，而且又要像C#一样囊括非常多的基础类以及高级语言特性。我这篇文章准备列举在日常工作当中需要留意的C++细节，这些细节很重要，能够拉近你对C++的认识，同时也是面试当中考官非常想问的问题，那我们开始吧！

### 函数重载
```
class A
{
public:
    void f(int *const a)
    void f(int *const a) const
    void f(int const* a) //编译器编译的结果为void f(A *const this,int const *a)
    void f(int const* a) const //编译器编译的结果为void f(A const *this,int const *a)
}
```

### this指针

```
A a_obj;
int i_val = 0;
a_obj.f(&i_val);//编译器编译的结果为:A::f(&a_obj,&i_val);
```

### 函数参数压栈顺序

```
f(int a,float b,double c){}
f(1,0.f,0.0);//先push c，再push b，再push a，如下示意图 
 ______
|___a__|<---stack top 低地址 0x0025fa90 - 8 - 4
|___b__|
|___c__|<---stack bottom 高地址，比如0x0025fa90
```

### move constructor & std::move

```
1.move constructor的作用是将右值引用中的资源窃取过来,避免不必要的开销
2.std::move返回传入对象的右值引用
class string
{
    char* data;

public:

    string(const char* p)
    {
        size_t size = strlen(p) + 1;
        data = new char[size];
        memcpy(data, p, size);
    }
    ~string()
    {
        delete[] data;
    }

    string(const string& that)
    {
        size_t size = strlen(that.data) + 1;
        data = new char[size];
        memcpy(data, that.data, size);
    }
    string(string&& that)   // string&& is an rvalue reference to a string
    {
        data = that.data;
        that.data = nullptr;
    }
    string& operator=(string that)
    {
        std::swap(data, that.data);
        return *this;
    }
};
string strmy(){ return "return a"; }
int main()
{
    //以下语句的执行过程为
    //1.通过std::move将strmy()返回的变量转化成为右值引用
    //2.调用string(string&& that)来构造变量b
    string b(std::move(strmy()));
}
```

### C++ 对象的内存布局

关于C++内存模型，去看看这篇文章，在这里，我只把图片引用过来，以及给出一些我的总结。

* 单一的一般继承（带成员变量、虚函数、虚函数覆盖）

继承关系如下图所示：

![](images/dd01.jpg)

内存模型如下图所示：

![](images/dd02.jpg)

在单一继承的模式下，基类，派生类，均共用同一张虚表，并且这一张虚表地址存储在基类的低位字节段（即使基类没有虚函数而派生类有虚函数，虚表的地址照样存储在基类的低位字节段）。注意，如果通过虚表获得成员函数指针是f1，然后在调用f1的时候this的地址就是0x1，而this->iParent的地址就应该是0x5（假设是32位机器）。如果这个f1中有一句代码是这样的：this->iParent = 1；那么会出错，因为0x5地址是不可写的，肯定被操作系统占用了。

* 单一的虚拟继承（带成员变量、虚函数、虚函数覆盖）
* 多重继承（带成员变量、虚函数、虚函数覆盖）

继承关系如下图所示：

![](images/dd03.jpg)

内存模型如下图所示：

![](images/dd04.jpg)

注意Base1，Base2，Base3均有相同的函数f，每一个函数都被Derive::f重载了。Base类都会有自己的虚表。

* 重复多重继承（带成员变量、虚函数、虚函数覆盖）

假设继承关系如下：

![](images/dd05.jpg)

内存模型如下：

![](images/dd06.jpg)

由上图可知：D继承B1，B1继承B，最终D overrider了f；D继承B2，B2继承B，最终D override了f；所以构造D的时候同时构造B1与B2，而B1与B2的内存模型互相独立，D继承f与id的时候会存在2份。

* 钻石型的虚拟多重继承（带成员变量、虚函数、虚函数覆盖）

假设继承关系如下,注意在继承的时候添加的**virtual** public B关键字：

![](images/dd07.jpg)

```
class B {……};
class B1 : virtual public B{……};
class B2: virtual public B{……};
class D : public B1, public B2{ …… };
```

内存模型如下：

```
+----------+
|B1::_vptr |-------------------------->+--------+
+----------+                           |D::f()  |
|B1::ib1:11|                           +--------+
+----------+                           |D::f1() |
|B1::cb1:1 |                           +--------+
+----------+                           |B1::Bf1 |
|B2::_vptr |------------>+---------+   +--------+
+----------+             |D::f()   |   |D::f2() |
|B2::ib2:12|             +---------+   +--------+
+----------+             |D::f2()  |   |D::Df() |
|B2::cb2:2 |             +---------+   +--------+
+----------+             |B2::Bf2()|   |1       |
|D::id:100 |             +---------+   +--------+
+----------+             |0        |
|D:cd:D    |             +---------+
+----------+
|B::_vptr  |->+-------+
+----------+  |D::f() |
|B:ib:0    |  +-------+
+----------+  |B::Bf()|
|B:cb:B    |  +-------+
+----------+  |0      |
|NULL      |  +-------+
+----------+
```

总结：virtual继承会使得B的内存模型只有一份，而且B的内存会在D内存的最后，同时B的内存有自己的虚函数表。

### Private Inheritance

C++ has a second means of implementing the has-a relationship: private inheritance.With private inheritance, public and protected members of the base class become private members of the derived class

### C++11 threads, locks and condition variables

* Threads

```
//basic usage
#include <thread>
 
void func()
{
   // do some work
}
 
int main()
{
   std::thread t(func);
   t.join();
 
   return 0;
}
```

```
//pass param to thread func
void func(int i, double d, const std::string& s)
{
    std::cout << i << ", " << d << ", " << s << std::endl;
}
 
int main()
{
   std::thread t(func, 1, 12.50, "sample");
   t.join();
 
   return 0;
}
```

```
//pass param as reference, use std::ref or std::cref
void func(int& a)
{
   a++;
}
 
int main()
{
   int a = 42;
   std::thread t(func, std::ref(a));
   t.join();
 
   std::cout << a << std::endl;
 
   return 0;
}
```

```
//catch exception in thread func
std::mutex                       g_mutex;
std::vector<std::exception_ptr>  g_exceptions;

void throw_function()
{
   throw std::exception("something wrong happened");
}

void func()
{
   try
   {
      throw_function();
   }
   catch(...)
   {
      std::lock_guard<std::mutex> lock(g_mutex);
      g_exceptions.push_back(std::current_exception());
   }
}

int main()
{
   g_exceptions.clear();

   std::thread t(func);
   t.join();

   for(auto& e : g_exceptions)
   {
      try 
      {
         if(e != nullptr)
         {
            std::rethrow_exception(e);
         }
      }
      catch(const std::exception& e)
      {
         std::cout << e.what() << std::endl;
      }
   }

   return 0;
}
```

* Locks

```
/*
mutex: provides the core functions lock() and unlock() and the non-blocking try_lock() method that returns if the mutex is not available.
*/
/*
recursive_mutex: allows multiple acquisitions of the mutex from the same thread.
*/
/*
timed_mutex: similar to mutex, but it comes with two more methods try_lock_for() and try_lock_until() that try to acquire the mutex for a period of time or until a moment in time is reached.
*/
/*
recursive_timed_mutex: is a combination of timed_mutex and recusive_mutex.
*/

#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
 
std::mutex g_lock;
 
void func()
{
    g_lock.lock();
 
    std::cout << "entered thread " << std::this_thread::get_id() << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(rand() % 10));
    std::cout << "leaving thread " << std::this_thread::get_id() << std::endl;
 
    g_lock.unlock();
}
 
int main()
{
    srand((unsigned int)time(0));
 
    std::thread t1(func);
    std::thread t2(func);
    std::thread t3(func);
 
    t1.join();
    t2.join();
    t3.join();
 
    return 0;
}
```

```
//use std::recursive_mutex to avoid dead lock
template <typename T>
class container 
{
    //std::mutex _lock; //dead lock will happen when uncomment this code
    std::recursive_mutex _lock;
    std::vector<T> _elements;
public:
    void add(T element) 
    {
        _lock.lock();
        _elements.push_back(element);
        _lock.unlock();
    }
 
    void addrange(int num, ...)
    {
        va_list arguments;
 
        va_start(arguments, num);
 
        for (int i = 0; i < num; i++)
        {
            _lock.lock();
            add(va_arg(arguments, T));
            _lock.unlock();
        }
 
        va_end(arguments); 
    }
 
    void dump()
    {
        _lock.lock();
        for(auto e : _elements)
            std::cout << e << std::endl;
        _lock.unlock();
    }
};
 
void func(container<int>& cont)
{
    cont.addrange(3, rand(), rand(), rand());
}
 
int main()
{
    srand((unsigned int)time(0));
 
    container<int> cont;
 
    std::thread t1(func, std::ref(cont));
    std::thread t2(func, std::ref(cont));
    std::thread t3(func, std::ref(cont));
 
    t1.join();
    t2.join();
    t3.join();
 
    cont.dump();
 
    return 0;
}
```

```
/*
lock_guard: when the object is constructed it attempts to acquire ownership of the mutex (by calling lock()) and when the object is destructed it automatically releases the mutex (by calling unlock()). This is a non-copyable class.
*/
/*
unique_lock: is a general purpose mutex wrapper that unlike lock_quard also provides support for deferred locking, time locking, recursive locking, transfer of lock ownership and use of condition variables. This is also a non-copyable class, but it is moveable.
*/
template <typename T>
class container 
{
    mutable std::recursive_mutex _lock; //use mutable to mark the member variable as modifiable
    std::vector<T> _elements;
public:
    void add(T element) 
    {
        std::lock_guard<std::recursive_mutex> locker(_lock);
        _elements.push_back(element);
    }
 
    void addrange(int num, ...)
    {
        va_list arguments;
 
        va_start(arguments, num);
 
        for (int i = 0; i < num; i++)
        {
            std::lock_guard<std::recursive_mutex> locker(_lock);
            add(va_arg(arguments, T));
        }
 
        va_end(arguments); 
    }
 
    void dump() const
    {
        std::lock_guard<std::recursive_mutex> locker(_lock);
        for(auto e : _elements)
            std::cout << e << std::endl;
    }
};
```

* Condition variables

```
```

#### C++11（10 features）

* auto

```
//use compiler to deduce the actual type of a variable that is being declared from its initializer
auto i = 42;        // i is an int
auto l = 42LL;      // l is an long long
auto p = new foo(); // p is a foo*

//using auto usually means less code
std::map<std::string, std::vector<int>> map;
for(auto it = begin(map); it != end(map); ++it) 
{
}

//auto cannot be used as the return type of a function
//you can use auto in place of the return type of function, 
//but in this case the function must have a trailing return type -> decltype(t1 + t2)
template <typename T1, typename T2>
auto compose(T1 t1, T2 t2) -> decltype(t1 + t2)
{
   return t1+t2;
}
auto v = compose(2, 3.14); // v's type is double
```

* nullptr

```
//nullptr is c++ key word
//used nullptr to avoid implicit convert to int type
void foo(int* p) {}
void foo(int p) {}

void bar(std::shared_ptr<int> p) {}

int* p1 = NULL;
int* p2 = nullptr;   
if(p1 == p2)
{
}

foo(nullptr);//use void foo(int* p) {}
foo(NULL);//use void foo(int p) {} which NULL is converted to int
bar(nullptr);

bool f = nullptr;// the value of f is false
int i = nullptr; // error: A native nullptr can only be converted to bool or, using reinterpret_cast, to an integral type
```

* Range-based for loops

```
//used for each when you don't care about indexes
std::map<std::string, std::vector<int>> map;
std::vector<int> v;
v.push_back(1);
v.push_back(2);
v.push_back(3);
map["one"] = v;

for(const auto& kvp : map) 
{
  std::cout << kvp.first << std::endl;

  for(auto v : kvp.second)
  {
     std::cout << v << std::endl;
  }
}

int arr[] = {1,2,3,4,5};
for(int& e : arr) 
{
  e = e*e;
}
```

* Override and final

```
//1.use override to indicate that you override some function from base class
//2.use final to mark some function, in order to make them impossible to override any more (down the hierarchy)
//3.both keyword override & final could be used in derived class


//before c++ 11
//some one would override f like below
//but below example actually overload the f and you may be ignore it
//you expectations is override f
class B 
{
public:
   virtual void f(short) {std::cout << "B::f" << std::endl;}
};

class D : public B
{
public:
   virtual void f(int) {std::cout << "D::f" << std::endl;}
};

//with c++ 11
//you can handle like this
//when you compile, the compiler will trigger an error like:
//     'D::f' : method with override specifier 'override' did not override any base class methods
class B 
{
public:
   virtual void f(short) {std::cout << "B::f" << std::endl;}
};

class D : public B
{
public:
   virtual void f(int) override final {std::cout << "D::f" << std::endl;}
};

```

* Strongly-typed enums

```
//this kind of newly enums 
//no longer export their enumerators in the surrounding scope
//no longer implicitly converted to integral types
//have a user-specified underlying type (a feature also added for traditional enums)
//you must use Options:: to ref the enum value
enum class Options {None, One, All};
Options o = Options::All;
int o_int = o;//will cause compile error
```

* Smart pointers 

```
//1 unique_ptr<T> & std::move
void foo(int* p)
{
   std::cout << *p << std::endl;
}
std::unique_ptr<int> p1(new int(42));
std::unique_ptr<int> p2 = std::move(p1); // transfer ownership
std::unique_ptr<int> p2 = p1;// compile error copy assignment operator is hidden by compiler

if(p1)
  foo(p1.get());

(*p2)++;

if(p2)
  foo(p2.get());

//result is : 43

//2 shared_ptr & std::make_shared<T>
//make_shared<T> is a non-member function and has the advantage of allocating memory 
//for the shared object and the smart pointer with a single allocation, 
//as opposed to the explicit construction of a shared_ptr via the contructor, that requires at least two allocations.
void foo(std::shared_ptr<int> p, int init)
{
   *p = init;
}
foo(std::shared_ptr<int>(new int(42)), seed());

//3 weak_ptr<T>
auto p = std::make_shared<int>(42);
std::weak_ptr<int> wp = p;

{
  auto sp = wp.lock();
  std::cout << *sp << std::endl;
}

p.reset();

if(wp.expired())
  std::cout << "expired" << std::endl;

//result is : expired
```

* Lambdas

```
std::vector<int> v;
v.push_back(1);
v.push_back(2);
v.push_back(3);

//anonymous functions:[](int n) {std::cout << n << std::endl;}
std::for_each(std::begin(v), std::end(v), [](int n) {std::cout << n << std::endl;});
//inferred from return type by single return statement
auto is_odd = [](int n) {return n%2==1;};
auto pos = std::find_if(std::begin(v), std::end(v), is_odd);
if(pos != std::end(v))
  std::cout << *pos << std::endl;
```

* non-member begin() and end()

```
//non-member begin() and end() functions promoting uniformity, consistency and enabling more generic programming
//they work with all STL containers 
//overloads for C-like arrays are also provided.
```

* static_assert and type traits

```
//1 static_assert performs an assertion check at compile-time not at run-time
//2 These are a series of classes that provide information about types at compile time. They are available in the <type_traits> header

//add is supposed to work only with integral types
template <typename T1, typename T2>
auto add(T1 t1, T2 t2) -> decltype(t1 + t2)
{
   static_assert(std::is_integral<T1>::value, "Type T1 must be integral");
   static_assert(std::is_integral<T2>::value, "Type T2 must be integral");

   return t1 + t2;
}
//when compile below code,error should show
std::cout << add(1, 3.14) << std::endl;
std::cout << add("one", 2) << std::endl;

/*****************
error C2338: Type T2 must be integral
see reference to function template instantiation 'T2 add<int,double>(T1,T2)' being compiled
   with
   [
      T2=double,
      T1=int
   ]
error C2338: Type T1 must be integral
see reference to function template instantiation 'T1 add<const char*,int>(T1,T2)' being compiled
   with
   [
      T1=const char *,
      T2=int
   ]
*******************/
```

* Move semantics

```
//1 move constructor & move assignment operator
//2 std::move
template <typename T>
class Buffer 
{
   std::string          _name;
   size_t               _size;
   std::unique_ptr<T[]> _buffer;

public:
   // default constructor
   Buffer():
      _size(16),
      _buffer(new T[16])
   {}

   // constructor
   Buffer(const std::string& name, size_t size):
      _name(name),
      _size(size),
      _buffer(new T[size])
   {}

   // copy constructor
   Buffer(const Buffer& copy):
      _name(copy._name),
      _size(copy._size),
      _buffer(new T[copy._size])
   {
      T* source = copy._buffer.get();
      T* dest = _buffer.get();
      std::copy(source, source + copy._size, dest);
   }

   // copy assignment operator
   Buffer& operator=(const Buffer& copy)
   {
      if(this != &copy)
      {
         _name = copy._name;

         if(_size != copy._size)
         {
            _buffer = nullptr;
            _size = copy._size;
            _buffer = _size > 0 > new T[_size] : nullptr;
         }

         T* source = copy._buffer.get();
         T* dest = _buffer.get();
         std::copy(source, source + copy._size, dest);
      }

      return *this;
   }

   // move constructor
   Buffer(Buffer&& temp):
      _name(std::move(temp._name)),
      _size(temp._size),
      _buffer(std::move(temp._buffer))
   {
      temp._buffer = nullptr;
      temp._size = 0;
   }

   // move assignment operator
   Buffer& operator=(Buffer&& temp)
   {
      assert(this != &temp); // assert if this is not a temporary

      _buffer = nullptr;
      _size = temp._size;
      _buffer = std::move(temp._buffer);

      _name = std::move(temp._name);

      temp._buffer = nullptr;
      temp._size = 0;
      
      return *this;
   }
};

template <typename T>
Buffer<T> getBuffer(const std::string& name) 
{
   Buffer<T> b(name, 128);
   return b;
}
int main()
{
   Buffer<int> b1;
   Buffer<int> b2("buf2", 64);
   Buffer<int> b3 = b2;
   Buffer<int> b4 = getBuffer<int>("buf4");//call move constructor Buffer(Buffer&& temp) cause of getBuffer<int>("buf4") return rvalue
   b1 = getBuffer<int>("buf5");//call move assignment operator cause of getBuffer<int>("buf4") return rvalue
   return 0;
}
```
