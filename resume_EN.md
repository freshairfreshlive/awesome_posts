```
Name:Silong Zheng          Gender:Male                   Age:28(born at May 18(th),1988)
Degree:Undergraduate       Phone Number:+8618717942545   Personal Projects: https://www.digolds.top
Work Experience:7+ years   Location: Pudong,Shanghai     e-mail:zheng_silong@126.com
```

# Education(2007/9-2011/7)

```
B.S.,Mathematics,Xian Electronics Science and Technology University
```

# Work Experience(2011/7-Now)

```
I�d developed software more than 7 years in medical industry. During the 7 years, I gained extensive experience in following aspects. 

>>Team & project management by using agile development methodologies
>>Proficient in building software applications in C/C++, Python on a Linux environment
>>Knowledge of professional software engineering practices for the full software development life cycle
>>Strong verbal and written communication skills in Chinese and English
>>Familiar with website development knowledge
```

# Projects
**2017/8 - Now**
> **Company**: MinFound Medical **Position**:Team Leader

> **Product**: AI Medical Workstation **Responsibility**:

> * Lead team to develop AI medical workstation system
> * Cooperate with outside partner to intergrate AI module for training Medical images data
> * Building up CICD enviroment with jenkins

**2016/3 - 2017/8**
> **Company**: Elekta **Position**:Senior Engineer

> **Product**: TPS **Responsibility**:

> * Cooperate with US team to discuss requirement
> * Design & implement a micro service component(couch) to TPS
> * Take advantage of tensorflow to develop u-net model to train medical images for segmentation

**2015/8 - 2016/3**
> **Start up**: Shenzhen Mvisioner **Position**:Founder

> **Product**: Distributed medical image workstation **Responsibility**:

> * Build up local repository by using SVN
> * Used C#,C++/C,OpenGL to build up distributed medical image workstation
> * Build up R&D development team from 0 to 1
> * Using agile for daily routine

**2014/6 - 2015/8**
> **Company**: Shanghai Time-medical Company **Position**: Engineer(Image Processing)

> **Product**: Distributed medical image workstation **Responsibility**:

> * Build a platform in order to accelerate development of Pulse Sequence and Image Reconstruction,in this platform I use C#,CLR C++,C++/C,CUDA
> * Research the image processing algorithm with the help of Matlab,and finally produce an easy use toolbox about imaging processing

**2011/8-2014/6**
> **Company**: Shanghai United-imaging Company **Position**: Engineer

> **Product**: Distributed medical image workstation **Responsibility**:

> * Implement 3D medical image render application
> * Develop 3D Render engine by Opengl & GLSL
> * Develop Patient Administration System by MVVM

# Skill Tags(2011/7-Now)

> * Tools: VS2010, TFS, jenkins, shell, git, svn
> * System API and SDK: CUDA, .NET, WPF, STL, OpenGL, zeromq, redis, nginx, tensorflow, vue, uikit
> * Software design method: Parallel Computing, Oriented-Object, Design Pattern, Multi-thread, Data structure, Template, Distributed system architected
> * Language: C/C++, C#, JavaScript, Python
> * Database: Mysql, redis, couch db